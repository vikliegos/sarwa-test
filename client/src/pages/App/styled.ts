import styled from 'styled-components'

export const AppWrapper = styled.div`
  padding: 40px 80px;
  width: 100vw;
`
