import { MutableRefObject, useEffect } from 'react'

type RefType = MutableRefObject<Element | null>

export const useClickOutside = (
  refs: RefType | RefType[],
  onOutsideClick: VoidFunction,
) => {
  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (Array.isArray(refs)) {
        if (
          !refs.some((ref) => ref.current && ref.current.contains(event.target as Node))
        ) {
          onOutsideClick()
        }
      } else {
        if (refs.current && !refs.current.contains(event.target as Node)) {
          onOutsideClick()
        }
      }
    }

    document.addEventListener('mousedown', handleClickOutside)
    return () => {
      document.removeEventListener('mousedown', handleClickOutside)
    }
  }, [onOutsideClick, refs])
}
