import { formatMoney } from '../formatMoney'

describe('formatMoney', () => {
  describe('when amount contains decimals', () => {
    it('should correctly format and show decimals', () => {
      expect(formatMoney(2032)).toBe('AED 20.32')
    })
  })

  describe('when amount does not contain decimals', () => {
    it('should correctly format and show decimals', () => {
      expect(formatMoney(2000)).toBe('AED 20')
    })
  })
})
