import accounts from './accounts.json'
import { Account } from '../types'
import { transformArrayToMap } from '../utils'

type AccountsManager = {
  accounts: Map<string, Account>
  changeAccount: (newAccountData: Account) => void
  getAccounts: () => Account[]
  getAccount: (accountId: string) => Account
}

export const accountsManager: AccountsManager = {
  accounts: transformArrayToMap(accounts as Account[], 'id'),

  changeAccount(newAccountData: Account) {
    this.accounts.set(newAccountData.id, newAccountData)
  },

  getAccounts() {
    return Array.from(this.accounts.values())
  },

  getAccount(accountId: string) {
    return this.accounts.get(accountId)
  },
}
