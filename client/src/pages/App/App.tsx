import { Accounts, AccountsTotal } from '../../containers'
import { AppWrapper } from './styled'

export function App() {
  return (
    <AppWrapper>
      <h2>Accounts Total</h2>
      <AccountsTotal />
      <br />
      <h2>Accounts</h2>
      <Accounts />
    </AppWrapper>
  )
}
