import { screen, render } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import { noop } from '../../../../utils'
import { AccountsStatusFilter } from '../AccountsStatusFilter'

describe('AccountsStatusFilter', () => {
  describe('when checked FUNDED and CLOSED checkboxes', () => {
    const setup = ({ onClose = noop, onChange = noop } = {}) => {
      render(<AccountsStatusFilter onClose={onClose} onChange={onChange} />)

      userEvent.click(screen.getByText('FUNDED'))
      userEvent.click(screen.getByText('CLOSED'))
    }

    it('should make them active', () => {
      // given / when
      setup()

      // then
      expect(screen.getByRole('checkbox', { name: 'FUNDED' })).toBeChecked()
      expect(screen.getByRole('checkbox', { name: 'CLOSED' })).toBeChecked()
    })

    describe('when unchecked FUNDED', () => {
      it('should leave only CLOSED as active', () => {
        // given
        setup()

        // when
        userEvent.click(screen.getByText('FUNDED'))

        // then
        expect(screen.getByRole('checkbox', { name: 'FUNDED' })).not.toBeChecked()
        expect(screen.getByRole('checkbox', { name: 'CLOSED' })).toBeChecked()
      })
    })

    describe('when hit Apply button', () => {
      it('should call onChange with selected statuses and onClose', () => {
        // given
        const handleClose = jest.fn()
        const handleChange = jest.fn()

        setup({ onClose: handleClose, onChange: handleChange })

        // when
        userEvent.click(screen.getByText('Apply'))

        // then
        expect(handleChange).toHaveBeenCalledWith(['funded', 'closed'])
        expect(handleClose).toHaveBeenCalled()
      })
    })
  })
})
