const DECIMAL_BASE = 10
const FIAT_CURRENCY_FRACTION = 2
const DEFAULT_CURRENCY = 'AED'

const prepareIntlMoneyFormatter = (currency: string) =>
  new Intl.NumberFormat('en', {
    style: 'currency',
    currency,
    minimumFractionDigits: 0,
  })

export const formatMoney = (amount: number, currency: string = DEFAULT_CURRENCY) => {
  const intlMoneyFormatted = prepareIntlMoneyFormatter(currency)

  return intlMoneyFormatted.format(
    amount / Math.pow(DECIMAL_BASE, FIAT_CURRENCY_FRACTION),
  )
}
