import styled from 'styled-components'

export const AccountsTotalParagraph = styled.p`
  font-size: 1.25rem;
`
