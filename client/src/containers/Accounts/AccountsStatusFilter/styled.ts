import styled from 'styled-components'

export const AccountsStatusFilterList = styled.ul`
  padding-left: 12px;
  list-style: none;

  > li {
    padding-top: 8px;
    padding-bottom: 8px;
  }
`
