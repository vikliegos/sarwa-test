import axios from 'axios'

const PORT = 8080

export const setupAxios = () => {
  axios.defaults.baseURL = `http://localhost:${PORT}`
}
