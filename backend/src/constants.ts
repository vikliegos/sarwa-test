export enum StatusCode {
  AccountIdInvalid = 1000,
  AccountIsClosed = 1001,
  StatusDoesNotMatch = 1002,
  AccountBalanceNonEmpty = 1003,
}

export enum HttpCode {
  Ok = 200,
  BadRequest = 400,
  Forbidden = 403,
  NotFound = 404,
  InternalServerError = 500,
}
