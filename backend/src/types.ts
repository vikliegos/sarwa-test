export type AccountStatus = 'pending' | 'approved' | 'funded' | 'closed'

export interface Account {
  id: string
  status: AccountStatus
  balance: number
}

export type AccountsFilter = {
  statuses: AccountStatus[]
}

export type AccountTotalOptions = {
  amount: number
} & {
  [status in AccountStatus]: number
}
