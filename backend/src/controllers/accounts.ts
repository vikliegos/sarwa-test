import { AccountsFilter, AccountStatus, AccountTotalOptions } from '../types'
import { accountsManager } from '../db'
import { StatusCode } from '../constants'
import { ApiError } from '../utils'

export const getAccounts = (filter?: AccountsFilter) => {
  const accounts = accountsManager.getAccounts()

  if (filter) {
    const statusFilter = new Set(filter.statuses)

    return accounts.filter((account) => statusFilter.has(account.status))
  }

  return accounts
}

export const getTotalOfAccounts = () => {
  const accounts = accountsManager.getAccounts()

  const total: AccountTotalOptions = {
    amount: 0,
    approved: 0,
    closed: 0,
    funded: 0,
    pending: 0,
  }

  for (const account of accounts) {
    total.amount += account.balance
    total[account.status]++
  }

  return total
}

const getAllowedStatuses = (currentStatus: AccountStatus, balance: number) => {
  switch (currentStatus) {
    case 'approved':
      return balance > 0 ? ['funded', 'closed'] : ['closed']
    case 'funded':
      return ['closed']
    case 'pending':
      return ['approved', 'closed']
    default:
      return null
  }
}

export const changeAccountStatus = (accountId: string, newStatus: AccountStatus) => {
  const currentAccount = accountsManager.getAccount(accountId)

  if (!currentAccount) {
    throw new ApiError({
      code: StatusCode.AccountIdInvalid,
      message: 'given account id is incorrect',
    })
  }

  const allowedStatuses = getAllowedStatuses(
    currentAccount.status,
    currentAccount.balance,
  )

  if (allowedStatuses) {
    if (allowedStatuses.includes(newStatus)) {
      if (currentAccount.status === 'funded' && currentAccount.balance !== 0) {
        throw new ApiError({
          code: StatusCode.AccountBalanceNonEmpty,
          message: 'status can not be changed when account has non empty balance',
        })
      }

      const newAccountData = { ...currentAccount, status: newStatus }

      accountsManager.changeAccount(newAccountData)

      return newAccountData
    }

    throw new ApiError({
      code: StatusCode.StatusDoesNotMatch,
      message: 'given status is not allowed for given account',
    })
  }

  throw new ApiError({
    code: StatusCode.AccountIsClosed,
    message: 'status can not be changed',
  })
}
