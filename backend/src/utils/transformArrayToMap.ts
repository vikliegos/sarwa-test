export function transformArrayToMap<T>(items: T[], keyProperty: string) {
  return items.reduce((mapAccumulator, item) => {
    mapAccumulator.set(item[keyProperty], item)

    return mapAccumulator
  }, new Map())
}
