import { transformArrayToMap } from '../transformArrayToMap'

test('transformArrayToMap', () => {
  expect(
    transformArrayToMap(
      [
        { id: '1', value: 'abc' },
        { id: '2', value: 'def' },
      ],
      'id',
    ),
  ).toEqual(
    new Map().set('1', { id: '1', value: 'abc' }).set('2', { id: '2', value: 'def' }),
  )
})
