import { useQuery } from 'react-query'

import { fetchAccounts } from '../../api'
import { QueryKey } from '../../constants'
import { AccountStatus } from '../../types'

export const useQueryAccountsData = (selectedStatuses?: AccountStatus[]) => {
  const { data } = useQuery(
    [QueryKey.Accounts, selectedStatuses],
    () => fetchAccounts(selectedStatuses),
    {
      refetchOnWindowFocus: false,
      keepPreviousData: true,
    },
  )

  return data
}
