# Steps to run the app:

1. Install dependencies with yarn in both backend and client services
2. Run `yarn start-all` in client


## More detailed descriptions for the commands are in the `backend/README.md` and in the `client/README.md`