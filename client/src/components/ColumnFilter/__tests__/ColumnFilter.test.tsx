import { useRef } from 'react'
import { screen, render } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import { ColumnFilter } from '../ColumnFilter'

const TestComponent = ({ onClose }: { onClose: VoidFunction }) => {
  const anchorRef = useRef(null)

  return (
    <div>
      <div>External element</div>
      <div ref={anchorRef}>Anchor</div>
      <ColumnFilter anchorRef={anchorRef} open onClose={onClose}>
        <span>Filter content</span>
      </ColumnFilter>
    </div>
  )
}

describe('ColumnFilter', () => {
  describe('when anchorRef clicked', () => {
    it('should not call onClose', () => {
      // given
      const onClose = jest.fn()

      render(<TestComponent onClose={onClose} />)

      // when
      userEvent.click(screen.getByText('Anchor'))

      // then
      expect(onClose).not.toHaveBeenCalled()
    })
  })

  describe('when clicked inside ColumnFilter', () => {
    it('should not call onClose', () => {
      // given
      const onClose = jest.fn()

      render(<TestComponent onClose={onClose} />)

      // when
      userEvent.click(screen.getByText('Filter content'))

      // then
      expect(onClose).not.toHaveBeenCalled()
    })
  })

  describe('when clicked outside ColumnFilter', () => {
    it('should call onClose', () => {
      // given
      const onClose = jest.fn()

      render(<TestComponent onClose={onClose} />)

      // when
      userEvent.click(screen.getByText('External element'))

      // then
      expect(onClose).toHaveBeenCalled()
    })
  })
})
