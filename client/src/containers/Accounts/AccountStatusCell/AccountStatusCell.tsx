import { ChangeEventHandler, FC } from 'react'

import { Account, AccountStatus } from '../../../types'
import { useChangeAccountStatus } from '../../../hooks'

type Props = {
  account: Account
}

export const getOptionsForAccountStatus = (
  accountStatus: AccountStatus,
  balance: number,
): AccountStatus[] | null => {
  switch (accountStatus) {
    case 'approved':
      return ['closed']
    case 'funded':
      return balance === 0 ? ['closed'] : null
    case 'pending':
      return ['approved', 'closed']
    default:
      return null
  }
}

export const AccountStatusCell: FC<Props> = ({ account }) => {
  const changeAccountStatus = useChangeAccountStatus()

  const accountStatus = account.status

  const optionsForAccountStatus = getOptionsForAccountStatus(
    accountStatus,
    account.balance,
  )

  if (!optionsForAccountStatus) {
    return <>{accountStatus.toUpperCase()}</>
  }

  const handleStatusChange: ChangeEventHandler<HTMLSelectElement> = (event) => {
    const newStatus = event.target.value.toLowerCase() as AccountStatus

    changeAccountStatus({
      accountId: account.id,
      newStatus,
    })
  }

  return (
    <select value={accountStatus} onChange={handleStatusChange}>
      <option>{accountStatus.toUpperCase()}</option>
      {optionsForAccountStatus.map((option) => (
        <option key={option}>{option.toUpperCase()}</option>
      ))}
    </select>
  )
}
