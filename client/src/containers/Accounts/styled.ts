import styled from 'styled-components'

export const AccountsTable = styled.table`
  width: 100%;
  max-width: 800px;
  border-collapse: collapse;
`

export const AccountsTableBodyRow = styled.tr`
  border-bottom: 1px solid #545454;
`

export const AccountsTableBodyCell = styled.td`
  padding: 12px 16px;
`
