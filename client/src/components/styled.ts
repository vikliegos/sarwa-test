import styled from 'styled-components'

export const FilterApplyButton = styled.button`
  padding: 4px 12px;
  background: #f6f6f6;
  cursor: pointer;
  margin-bottom: 12px;
`
