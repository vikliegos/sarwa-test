import axios from 'axios'
import qs from 'qs'

import { Account, AccountDataDto, AccountStatus } from '../types'

export const fetchAccounts = async (selectedStatuses?: AccountStatus[]) => {
  const { data } = await axios.get<AccountDataDto>('/accounts', {
    params: { status: selectedStatuses },
    paramsSerializer: (params) => qs.stringify(params, { arrayFormat: 'repeat' }),
  })

  return data
}

export const changeAccountStatus = async ({
  accountId,
  newStatus,
}: {
  accountId: string
  newStatus: AccountStatus
}) => {
  const { data } = await axios.put<{ account: Account }>('/account-status', {
    id: accountId,
    newStatus,
  })

  return data
}
