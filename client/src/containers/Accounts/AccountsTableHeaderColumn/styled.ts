import styled from 'styled-components'

export const AccountsTableHeaderColumnBase = styled.th`
  text-align: left;
  color: #545454;
  background-color: #f6f6f6;
  height: 60px;
  padding-left: 16px;
  padding-right: 16px;
  position: relative;

  &:not(:last-of-type) {
    border-right: 2px solid #fff;
  }
`

export const AccountsTableHeaderColumnContent = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
`
