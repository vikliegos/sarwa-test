import { FC, useState } from 'react'

import { Checkbox, FilterApplyButton } from '../../../components'
import { AccountStatus } from '../../../types'
import { AccountsStatusFilterList } from './styled'

type Props = {
  onClose: VoidFunction
  onChange: (selectedStatuses: AccountStatus[]) => void
}

export const AccountsStatusFilter: FC<Props> = ({ onClose, onChange }) => {
  const [selectedStatuses, setSelectedStatuses] = useState<Set<AccountStatus>>(new Set())

  const handleCheckboxChange = (status: AccountStatus) => (selected: boolean) => {
    if (selected) {
      setSelectedStatuses(new Set(selectedStatuses).add(status))
    } else {
      const newStatuses = new Set(selectedStatuses)
      newStatuses.delete(status)
      setSelectedStatuses(newStatuses)
    }
  }

  const handleSubmit = () => {
    onChange(Array.from(selectedStatuses.values()))
    onClose()
  }

  return (
    <>
      <AccountsStatusFilterList>
        <li>
          <Checkbox
            defaultChecked={selectedStatuses.has('approved')}
            onChange={handleCheckboxChange('approved')}
          >
            APPROVED
          </Checkbox>
        </li>
        <li>
          <Checkbox
            defaultChecked={selectedStatuses.has('pending')}
            onChange={handleCheckboxChange('pending')}
          >
            PENDING
          </Checkbox>
        </li>
        <li>
          <Checkbox
            defaultChecked={selectedStatuses.has('funded')}
            onChange={handleCheckboxChange('funded')}
          >
            FUNDED
          </Checkbox>
        </li>
        <li>
          <Checkbox
            defaultChecked={selectedStatuses.has('closed')}
            onChange={handleCheckboxChange('closed')}
          >
            CLOSED
          </Checkbox>
        </li>
      </AccountsStatusFilterList>
      <FilterApplyButton onClick={handleSubmit}>Apply</FilterApplyButton>
    </>
  )
}
