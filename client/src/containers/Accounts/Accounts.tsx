import { FC, useState, memo } from 'react'

import { useQueryAccountsData } from '../../hooks'
import { AccountStatus } from '../../types'
import { AccountRow } from './AccountRow'
import { AccountsStatusFilter } from './AccountsStatusFilter'
import { AccountsTableHeaderColumn } from './AccountsTableHeaderColumn'
import { AccountsTable } from './styled'

export const Accounts: FC = memo(() => {
  const [selectedStatuses, setSelectedStatuses] = useState<AccountStatus[]>([])

  const accountsData = useQueryAccountsData(selectedStatuses)

  return (
    <AccountsTable>
      <thead>
        <tr>
          <AccountsTableHeaderColumn title="ID" />
          <AccountsTableHeaderColumn
            title="Status"
            renderFilterContent={({ onClose }) => (
              <AccountsStatusFilter onChange={setSelectedStatuses} onClose={onClose} />
            )}
          />
          <AccountsTableHeaderColumn title="Balance" />
        </tr>
      </thead>
      <tbody>
        {accountsData?.accounts?.map((account) => (
          <AccountRow key={account.id} account={account} />
        ))}
      </tbody>
    </AccountsTable>
  )
})
