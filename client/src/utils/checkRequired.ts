export function checkRequired<T>(value: T) {
  if (value === null || value === undefined) {
    throw new Error('Required value can not be empty')
  }

  return value as NonNullable<T>
}
