import express, { Express, Request, Response } from 'express'
import cors from 'cors'

import { accountsController } from './controllers'
import { AccountsFilter, AccountStatus } from './types'
import { HttpCode } from './constants'
import { ApiError } from './utils'
import { getTotalOfAccounts } from './controllers/accounts'

const PORT = 8080

const app: Express = express()

app.use(express.json())
app.use(cors())

app.get('/accounts', (req: Request, res: Response) => {
  const query = req.query
  let filter: AccountsFilter | undefined

  if (query.status) {
    filter = {
      statuses: (Array.isArray(query.status)
        ? query.status
        : [query.status]) as AccountStatus[],
    }
  }

  const accounts = accountsController.getAccounts(filter)

  res.json({
    accounts,
    total: getTotalOfAccounts(),
  })
})

app.put('/account-status', (req: Request, res: Response) => {
  const body = req.body as { id: string; newStatus: AccountStatus }

  try {
    const account = accountsController.changeAccountStatus(body.id, body.newStatus)

    res.json({
      account,
    })
  } catch (e) {
    res.status(HttpCode.BadRequest).json(e as ApiError)
  }
})

app.listen(PORT, () => {
  console.log(`Server is running at http://localhost:${PORT}`)
})
