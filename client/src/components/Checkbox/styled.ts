import styled, { css } from 'styled-components'

export const CheckboxInputWrapper = styled.div`
  color: red;
  flex: 0 0 auto;
`

export const CheckboxInput = styled.input`
  border: 0;
  height: 0;
  width: 0;
  margin: -1px;
  padding: 0;
  overflow: hidden;
  white-space: nowrap;
  position: absolute;
`

export const CheckboxContainer = styled.label`
  position: relative;
  display: flex;
  align-items: center;
  cursor: pointer;
`

export const CheckboxBaseContainer = styled.div<{ checked: boolean }>`
  width: 16px;
  height: 16px;
  border: 1px solid #545454;

  ${({ checked }) =>
    checked &&
    css`
      background: red;

      &:after {
        display: block;
        content: '';
        position: absolute;
        left: 4px;
        top: 2px;
        width: 4px;
        height: 8px;
        border: solid white;
        border-width: 0 3px 3px 0;
        transform: rotate(45deg);
      }
    `};
`

export const CheckboxChildren = styled.span`
  margin-left: 8px;
`
