export type AccountStatus = 'pending' | 'approved' | 'funded' | 'closed'

export interface Account {
  id: string
  status: AccountStatus
  balance: number
}

export type AccountTotalOptions = {
  amount: number
} & {
  [status in AccountStatus]: number
}

export type AccountDataDto = {
  accounts: Account[]
  total: AccountTotalOptions
}
