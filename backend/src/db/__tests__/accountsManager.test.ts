import { accountsManager } from '../accountsManager'
import { Account } from '../../types'

jest.mock('../accountsManager', () => {
  const { ACCOUNTS_MAP } = jest.requireActual<any>('../../fixtures')
  const { accountsManager } = jest.requireActual<any>('../accountsManager')

  return {
    accountsManager: {
      accounts: ACCOUNTS_MAP,
      getAccounts: accountsManager.getAccounts,
      getAccount: accountsManager.getAccount,
      changeAccount: accountsManager.changeAccount,
    },
  }
})

describe('accountsManager', () => {
  describe('when account status changed', () => {
    it('should change accounts in object', () => {
      const newAccountData: Account = {
        id: 'ad76dfc9-a193-473a-91b0-1178ff4bd73e',
        balance: 4000000,
        status: 'funded',
      }

      accountsManager.changeAccount(newAccountData)

      expect(accountsManager.getAccount('ad76dfc9-a193-473a-91b0-1178ff4bd73e')).toBe(
        newAccountData,
      )
    })
  })
})
