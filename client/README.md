# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start-all`

Runs the app and the API in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

### `yarn start:ui`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `yarn start:api`

Runs the API under [http://localhost:8080](http://localhost:8080).

### `yarn lint`

Runs the ESLint in client.

### `yarn tsc`

Runs typescript check in client.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
