import { FC, MutableRefObject, PropsWithChildren, useRef } from 'react'

import { useClickOutside } from '../../hooks'
import { ColumnFilterContainer } from './styled'

type ColumnFilterProps = {
  anchorRef: MutableRefObject<Element | null>
  open: boolean
  onClose: VoidFunction
}

export const ColumnFilter: FC<PropsWithChildren<ColumnFilterProps>> = ({
  anchorRef,
  children,
  open,
  onClose,
}) => {
  const filterRef = useRef<HTMLDivElement>(null)
  useClickOutside([anchorRef, filterRef], onClose)

  return (
    <ColumnFilterContainer ref={filterRef} open={open}>
      {children}
    </ColumnFilterContainer>
  )
}
