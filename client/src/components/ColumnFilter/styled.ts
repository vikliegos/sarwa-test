import styled from 'styled-components'

export const ColumnFilterContainer = styled.div<{ open: boolean }>`
  display: ${({ open }) => (open ? 'block' : 'none')};
  position: absolute;
  top: 100%;
  left: 0;
  right: 0;
  background: white;
  box-shadow: 0 0 20px -10px rgba(147, 147, 167, 1);
  text-align: center;
`
