import { FC } from 'react'
import { CheckboxBaseContainer } from './styled'

type Props = {
  checked: boolean
}

export const CheckboxBase: FC<Props> = ({ checked }) => {
  return <CheckboxBaseContainer checked={checked} />
}
