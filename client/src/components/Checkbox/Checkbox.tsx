import { FC, PropsWithChildren, useState } from 'react'

import {
  CheckboxChildren,
  CheckboxContainer,
  CheckboxInput,
  CheckboxInputWrapper,
} from './styled'
import { CheckboxBase } from './CheckboxBase'

type Props = {
  defaultChecked?: boolean
  onChange?: (checked: boolean) => void
}

export const Checkbox: FC<PropsWithChildren<Props>> = ({
  children,
  defaultChecked = false,
  onChange,
}) => {
  const [checked, setChecked] = useState(defaultChecked)

  const handleCheckboxChange = () => {
    onChange?.(!checked)
    setChecked((prev) => !prev)
  }

  return (
    <CheckboxContainer>
      <CheckboxInputWrapper>
        <CheckboxInput
          checked={checked}
          type="checkbox"
          onChange={handleCheckboxChange}
        />
        <CheckboxBase checked={checked} />
      </CheckboxInputWrapper>
      <CheckboxChildren>{children}</CheckboxChildren>
    </CheckboxContainer>
  )
}
