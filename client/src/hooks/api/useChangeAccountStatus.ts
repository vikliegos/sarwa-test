import { useMutation, useQueryClient } from 'react-query'

import { changeAccountStatus } from '../../api'
import { QueryKey } from '../../constants'

export const useChangeAccountStatus = () => {
  const queryClient = useQueryClient()

  const { mutate } = useMutation(changeAccountStatus, {
    onSuccess: () => {
      queryClient.invalidateQueries(QueryKey.Accounts)
    },
  })

  return mutate
}
