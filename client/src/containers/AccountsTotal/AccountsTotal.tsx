import { FC, memo } from 'react'

import { useQueryAccountsData } from '../../hooks'
import { formatMoney } from '../../utils'
import { AccountsTotalParagraph } from './styled'

const formatAccountsAmount = (amount: number) => {
  if (amount === 1) {
    return `${amount} account`
  }

  return `${amount} accounts`
}

export const AccountsTotal: FC = memo(() => {
  const accountsData = useQueryAccountsData()

  if (!accountsData) {
    return <p>Loading...</p>
  }

  const { total: totalOptions } = accountsData

  return (
    <div>
      <AccountsTotalParagraph>
        <b>Balance:</b> {formatMoney(totalOptions.amount)}
      </AccountsTotalParagraph>
      <AccountsTotalParagraph>
        <b>Approved:</b> {formatAccountsAmount(totalOptions.approved)}
      </AccountsTotalParagraph>
      <AccountsTotalParagraph>
        <b>Pending:</b> {formatAccountsAmount(totalOptions.pending)}
      </AccountsTotalParagraph>
      <AccountsTotalParagraph>
        <b>Funded:</b> {formatAccountsAmount(totalOptions.funded)}
      </AccountsTotalParagraph>
      <AccountsTotalParagraph>
        <b>Closed:</b> {formatAccountsAmount(totalOptions.closed)}
      </AccountsTotalParagraph>
    </div>
  )
})
