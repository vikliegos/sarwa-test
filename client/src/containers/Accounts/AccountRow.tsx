import { FC } from 'react'

import { Account } from '../../types'
import { formatMoney } from '../../utils'
import { AccountsTableBodyCell, AccountsTableBodyRow } from './styled'
import { AccountStatusCell } from './AccountStatusCell'

type Props = {
  account: Account
}

export const AccountRow: FC<Props> = ({ account }) => {
  return (
    <AccountsTableBodyRow>
      <AccountsTableBodyCell>{account.id}</AccountsTableBodyCell>
      <AccountsTableBodyCell>
        <AccountStatusCell account={account} />
      </AccountsTableBodyCell>
      <AccountsTableBodyCell>
        {account.balance > 0 ? formatMoney(account.balance) : 0}
      </AccountsTableBodyCell>
    </AccountsTableBodyRow>
  )
}
