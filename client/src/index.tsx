import * as React from 'react'
import { createRoot } from 'react-dom/client'
import { QueryClient, QueryClientProvider } from 'react-query'

import { setupAxios } from './config'
import { App } from './pages'

import './index.css'

setupAxios()

const rootElement = document.getElementById('root') as Element

const root = createRoot(rootElement)

const queryClient = new QueryClient()

root.render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <App />
    </QueryClientProvider>
  </React.StrictMode>,
)
