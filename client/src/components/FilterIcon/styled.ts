import styled from 'styled-components'

export const FilterIconBase = styled.svg`
  width: 24px;
  height: 24px;

  cursor: pointer;
`
