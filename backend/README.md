## Available Scripts

### `yarn start`

Runs the API under [http://localhost:8080](http://localhost:8080).

### `yarn test`

Runs the tests for backend service

### `yarn lint`

Runs ESLint in backend

### `yarn tsc`

Runs typescript check in backend