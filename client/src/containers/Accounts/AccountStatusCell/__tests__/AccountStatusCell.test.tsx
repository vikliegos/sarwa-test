import { screen, render } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import * as useChangeAccountStatusModule from '../../../../hooks/api/useChangeAccountStatus'
import { Account } from '../../../../types'
import { noop } from '../../../../utils'
import { AccountStatusCell } from '../AccountStatusCell'

const TEST_ACCOUNT: Account = {
  id: 'account-id',
  status: 'pending',
  balance: 0,
}

describe('AccountStatusCell', () => {
  describe('when account status is editable', () => {
    describe('when account status changes', () => {
      it('should call changeAccountStatus with selected status', () => {
        // given
        const changeAccountStatus = jest.fn()

        jest
          .spyOn(useChangeAccountStatusModule, 'useChangeAccountStatus')
          .mockReturnValue(changeAccountStatus)

        render(<AccountStatusCell account={TEST_ACCOUNT} />)

        // when
        userEvent.selectOptions(screen.getByRole('combobox'), ['APPROVED'])

        // then
        expect(changeAccountStatus).toHaveBeenCalledWith({
          accountId: 'account-id',
          newStatus: 'approved',
        })
      })
    })
  })

  describe('when status is not editable', () => {
    it('should not render combobox', () => {
      // given
      jest
        .spyOn(useChangeAccountStatusModule, 'useChangeAccountStatus')
        .mockReturnValue(noop)

      render(<AccountStatusCell account={{ ...TEST_ACCOUNT, status: 'closed' }} />)

      // then
      expect(screen.queryByRole('combobox')).not.toBeInTheDocument()
    })
  })
})
