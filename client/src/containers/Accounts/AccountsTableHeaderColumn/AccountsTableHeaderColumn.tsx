import { FC, PropsWithChildren, ReactNode, useRef, useState } from 'react'

import { ColumnFilter, FilterIcon } from '../../../components'
import { AccountsTableHeaderColumnBase, AccountsTableHeaderColumnContent } from './styled'

type Props = {
  title: string
  renderFilterContent?: ({ onClose }: { onClose: VoidFunction }) => ReactNode
}

export const getFilterLabel = (label: string) => `filter_${label}`

export const AccountsTableHeaderColumn: FC<PropsWithChildren<Props>> = ({
  title,
  children,
  renderFilterContent,
}) => {
  const [isFilterOpen, setFilterOpen] = useState(false)
  const filterAnchorRef = useRef<HTMLSpanElement>(null)

  const handleFilterClose = () => setFilterOpen(false)

  return (
    <AccountsTableHeaderColumnBase>
      <AccountsTableHeaderColumnContent>
        {title}
        {children}
        {renderFilterContent && (
          <>
            <span
              aria-label={getFilterLabel(title)}
              ref={filterAnchorRef}
              onClick={() => setFilterOpen((prev) => !prev)}
            >
              <FilterIcon />
            </span>
            <ColumnFilter
              anchorRef={filterAnchorRef}
              open={isFilterOpen}
              onClose={handleFilterClose}
            >
              {renderFilterContent({ onClose: handleFilterClose })}
            </ColumnFilter>
          </>
        )}
      </AccountsTableHeaderColumnContent>
    </AccountsTableHeaderColumnBase>
  )
}
