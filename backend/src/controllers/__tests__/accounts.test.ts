import { AccountStatus } from '../../types'
import { StatusCode } from '../../constants'
import { ACCOUNTS_MAP } from '../../fixtures'
import * as accountsController from '../accounts'

jest.mock('../../db', () => {
  const { accountsManager } = jest.requireActual<any>('../../db')
  const { ACCOUNTS_MAP } = jest.requireActual<any>('../../fixtures')

  return {
    accountsManager: {
      accounts: ACCOUNTS_MAP,
      getAccounts: accountsManager.getAccounts,
      getAccount: accountsManager.getAccount,
      changeAccount: jest.fn(),
    },
  }
})

describe('accounts', () => {
  describe('getAccounts', () => {
    describe('when no filter paste', () => {
      it('should return all accounts', () => {
        expect(accountsController.getAccounts()).toEqual(
          Array.from(ACCOUNTS_MAP.values()),
        )
      })
    })

    describe('when filter pasted', () => {
      it('should return filtered accounts with appropriate statuses', () => {
        const statuses: AccountStatus[] = ['pending', 'funded']

        expect(
          accountsController.getAccounts({
            statuses,
          }),
        ).toEqual([
          {
            id: '6df1f463-0caa-4ba4-aa3f-b97c99c10fcb',
            balance: 0,
            status: 'pending',
          },
          {
            id: '63adf5ec-58c2-4371-b317-d7275b68c34c',
            balance: 0,
            status: 'funded',
          },
          {
            id: '63adf5ec-58c2-4371-b317-d7275b68c35g',
            balance: 20000,
            status: 'funded',
          },
        ])
      })
    })
  })

  describe('changeAccountStatus', () => {
    describe('when account id is correct', () => {
      describe('when destination status is allowed', () => {
        it('should change status and return changed account', () => {
          expect(
            accountsController.changeAccountStatus(
              '6df1f463-0caa-4ba4-aa3f-b97c99c10fcb',
              'approved',
            ),
          ).toEqual({
            id: '6df1f463-0caa-4ba4-aa3f-b97c99c10fcb',
            balance: 0,
            status: 'approved',
          })
        })
      })

      describe('when destination status is not allowed', () => {
        it('should throw an error that status is not allowed', () => {
          expect(() =>
            accountsController.changeAccountStatus(
              '6df1f463-0caa-4ba4-aa3f-b97c99c10fcb',
              'funded',
            ),
          ).toThrowError(expect.objectContaining({ code: StatusCode.StatusDoesNotMatch }))
        })
      })

      describe('when current account status is closed', () => {
        it('should throw an error that status is closed', () => {
          expect(() =>
            accountsController.changeAccountStatus(
              'f3f96c55-3026-4fb2-865f-d29a5315e19e',
              'funded',
            ),
          ).toThrowError(expect.objectContaining({ code: StatusCode.AccountIsClosed }))
        })
      })

      describe('when current account status is pending and balance is not 0', () => {
        it('should throw an error that account balance is not empty', () => {
          expect(() =>
            accountsController.changeAccountStatus(
              '63adf5ec-58c2-4371-b317-d7275b68c35g',
              'closed',
            ),
          ).toThrowError(
            expect.objectContaining({ code: StatusCode.AccountBalanceNonEmpty }),
          )
        })
      })

      describe('when current account status is pending and balance is 0', () => {
        it('should change status and returned changed account', () => {
          expect(
            accountsController.changeAccountStatus(
              '63adf5ec-58c2-4371-b317-d7275b68c34c',
              'closed',
            ),
          ).toEqual({
            id: '63adf5ec-58c2-4371-b317-d7275b68c34c',
            balance: 0,
            status: 'closed',
          })
        })
      })
    })
  })
})
