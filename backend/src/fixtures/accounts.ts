import { Account } from '../types'
import { transformArrayToMap } from '../utils'

export const ACCOUNTS_MAP: Map<string, Account> = transformArrayToMap(
  [
    {
      id: '6df1f463-0caa-4ba4-aa3f-b97c99c10fcb',
      balance: 0,
      status: 'pending',
    },
    {
      id: 'ad76dfc9-a193-473a-91b0-1178ff4bd73e',
      balance: 4000000,
      status: 'approved',
    },
    {
      id: '63adf5ec-58c2-4371-b317-d7275b68c34c',
      balance: 0,
      status: 'funded',
    },
    {
      id: '63adf5ec-58c2-4371-b317-d7275b68c35g',
      balance: 20000,
      status: 'funded',
    },
    {
      id: 'f3f96c55-3026-4fb2-865f-d29a5315e19e',
      balance: 0,
      status: 'closed',
    },
  ],
  'id',
)
