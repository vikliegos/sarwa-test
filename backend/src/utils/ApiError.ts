import { StatusCode } from '../constants'

type ApiErrorArgs = {
  code: StatusCode
  message: string
}

export class ApiError extends Error {
  code: StatusCode

  constructor(args: ApiErrorArgs) {
    super(args.message)

    this.code = args.code
    this.name = 'ApiError'
  }
}
