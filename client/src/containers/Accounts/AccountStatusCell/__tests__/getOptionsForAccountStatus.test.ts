import { getOptionsForAccountStatus } from '../AccountStatusCell'

describe('getOptionsForAccountStatus', () => {
  describe('when account is funded', () => {
    describe('when balance is more than 0', () => {
      it('should not have available options', () => {
        expect(getOptionsForAccountStatus('funded', 1000)).toBe(null)
      })
    })

    describe('when balance is 0', () => {
      it('should return close option', () => {
        expect(getOptionsForAccountStatus('funded', 0)).toEqual(['closed'])
      })
    })
  })

  describe('when account is approved', () => {
    it('should show only close as manual change option', () => {
      expect(getOptionsForAccountStatus('approved', 0)).toEqual(['closed'])
    })
  })
})
