import { screen, render } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

import { useQueryAccountsData } from '../../../hooks'
import { Accounts } from '../Accounts'
import { getFilterLabel } from '../AccountsTableHeaderColumn'

jest.mock('../../../hooks/api', () => ({
  useChangeAccountStatus: jest.fn(),
  useQueryAccountsData: jest.fn(() => ({
    accounts: [
      {
        id: '6df1f463-0caa-4ba4-aa3f-b97c99c10fcb',
        balance: 0,
        status: 'pending',
      },
      {
        id: 'ad76dfc9-a193-473a-91b0-1178ff4bd73e',
        balance: 40000,
        status: 'approved',
      },
      {
        id: '63adf5ec-58c2-4371-b317-d7275b68c34c',
        balance: 0,
        status: 'funded',
      },
      {
        id: '63adf5ec-58c2-4371-b317-d7275b68c31a',
        balance: 20000,
        status: 'funded',
      },
      {
        id: '63adf5ec-58c2-4371-b317-d7275b68c35d',
        balance: 0,
        status: 'closed',
      },
    ],
  })),
}))

const useQueryAccountsDataMock = useQueryAccountsData as jest.Mock

describe('Accounts', () => {
  describe('when selected status in status filter and clicked apply', () => {
    it('should call fetchAccounts with selected status', async () => {
      // given
      render(<Accounts />)

      // when
      userEvent.click(screen.getByLabelText(getFilterLabel('Status')))

      userEvent.click(screen.getByRole('checkbox', { name: 'APPROVED' }))
      userEvent.click(screen.getByRole('checkbox', { name: 'PENDING' }))

      userEvent.click(screen.getByText('Apply'))

      // then
      expect(useQueryAccountsDataMock).toHaveBeenCalledWith(['approved', 'pending'])
    })
  })
})
